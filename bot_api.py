# -*- coding: utf-8 -*-
import telebot

from config import Config

config = Config()

bot = telebot.TeleBot(config.token)

def send_bot_message_to_me(message):
    bot.send_message(config.default_chat_id, message)

def send_bot_message_to_chat(chat_id, message):
    bot.send_message(chat_id, message)




