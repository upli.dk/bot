import os
from configparser import ConfigParser


class Config(object):
    class __Config(object):
        def __init__(self, file_names='settings.cfg'):
            fileDir = os.path.dirname(os.path.realpath(__file__))

            self.config = ConfigParser()
            self._readed_config = self.config.read(os.path.join(fileDir, file_names ))

            self._token = self.config.get("server", "token")
            self._default_chat_id = self.config.get("server", "default_chat_id")
            self._my_email = self.config.get("server", "my_email")
            self._password = self.config.get("server", "password")
            self._username = self.config.get("server", "username")
            self._db_file_name = self.config.get("server", "db_file_name")
            self._socks5_proxy = self.config.get("server", "socks5_proxy")

        @property
        def token(self):
            return self._token

        @property
        def default_chat_id(self):
            return self._default_chat_id

        @property
        def my_email(self):
            return self._my_email

        @property
        def username(self):
            return self._username

        @property
        def password(self):
            return self._password

        @property
        def db_file_name(self):
            return self._db_file_name

        @property
        def socks5_proxy(self):
            if self._socks5_proxy:
                return {'https': self._socks5_proxy}

    instance = None

    def __init__(self, **arg):
        if not Config.instance:
            Config.instance = Config.__Config(*arg)

    def __getattr__(self, name):
        return getattr(self.instance, name)
