#!/usr/bin/python
# -*- coding: utf-8 -*-

import os

import argparse
import io
import urllib.request, urllib.parse, urllib.error
from hashlib import md5
from random import shuffle
from time import sleep

import telebot
import vk

from config import Config

config = Config()

session = vk.AuthSession(app_id='6112871', user_login=config.my_email,
                         user_password=config.password)
api = vk.API(session, v='5.73')
telebot.apihelper.proxy = config.socks5_proxy
old_id = False


def run(bot, src, chats):
    for source in list(src.values()):
        photo_raw = source.get_next_post()
        if photo_raw:
            for chat_id in chats:
                bot.send_photo(chat_id, photo_raw)


class PostSource(object):
    def __init__(self, group_name):
        self._old_id = ''
        self._owner_id = -api.groups.getById(group_ids=group_name)[0]["id"]

    def get_next_post(self):
        from datetime import datetime
        now = datetime.now()
        posts = api.wall.get(owner_id=self._owner_id, count=60,
                             offset=now.hour)

        for i in range(len(posts['items'])):
            post = posts['items'][i]
            if self._old_id != post['id'] and self._is_photo(post):
                photo_url = post['attachments'][0]['photo']['photo_1280']
                db_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), config.db_file_name)

                if is_duplicate(db_path, photo_url):
                    continue

                self._old_id = post['id']
                add_hash(db_path, photo_url)
                picture = urllib.request.urlopen(photo_url).read()
                return picture
        return None

    def get_post(self, number):
        posts = api.wall.get(owner_id=self._owner_id, count=100)

        post = posts[number]
        if self._is_photo(post):
            picture = io.StringIO(urllib.request.urlopen(
                post['attachment']['photo']['src_big']).read())
            return picture
        return None

    @staticmethod
    def _is_photo(post):
        return ('attachments' in post
                and len(post['attachments'])
                and 'type' in post['attachments'][0]
                and post['attachments'][0]['type'] == 'photo'
                and 'photo_1280' in post['attachments'][0]['photo']
                and post['attachments'][0]['photo']['photo_1280']
                and not post['marked_as_ads'])


def get_cmdline_args():
    parser = argparse.ArgumentParser(description='Бот для постинга из VK в '
                                                 'Telegram')
    parser.add_argument('-t', nargs='?', required=True, help='Bot Token')
    parser.add_argument('-c', nargs='*', required=True, help='Chat '
                                                             'Ids')
    parser.add_argument('-g', nargs='*', required=True, type=str,
                        help='Group Ids')
    parser.add_argument('-b', action='store_true',
                        help='To batch or not to batch')
    parser.add_argument('-a', action='store_true', help='not to batch')
    return parser.parse_args()


def batch(bot, src, chats):
    for source in list(src.values()):
        if batch:
            ints = list(range(100))
            shuffle(ints)
            for number in ints:
                try:
                    photo = source.get_post(number)
                    if photo:
                        for chat_id in chats:
                            bot.send_photo(chat_id, photo)
                            sleep(2)
                except Exception as e:
                    print((e.message))


def is_duplicate(db_path, url):
    pic_name = url.split('/')[-1]
    hash_url = md5(pic_name.encode('utf-8')).hexdigest()
    with open(db_path, 'r') as f:
        for line in f:
            if line.split('|')[0] == hash_url:
                return True
    return False


def add_hash(file_name, url):
    pic_name = url.split('/')[-1]
    hash_url = md5(pic_name.encode('utf-8')).hexdigest()
    with open(file_name, 'a') as f:
        f.write('{}|{}|{}\n'.format(hash_url, pic_name, url))


if __name__ == '__main__':
    args = get_cmdline_args()

    sources = {}
    for group_name in args.g:
        sources[group_name] = PostSource(group_name)

    chats = []
    for chat_id in args.c:
        chats.append(chat_id)

    bot = telebot.TeleBot(args.t)

    if args.b:
        batch(bot, sources, chats)
    if args.a:
        run(bot, sources, chats)
    else:
        while True:
            run(bot, sources, chats)
            sleep(60*45)
