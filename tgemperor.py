#!/usr/bin/python
# -*- coding: utf-8 -*-


import os
import subprocess

from yaml import Loader, load

import argparse


def get_cmdline_args():
    parser = argparse.ArgumentParser(description='Бот для постинга из VK в '
                                                 'Telegram')
    parser.add_argument('-s', nargs='?', type=str, help='Settings yaml file')

    return parser.parse_args()


if __name__ == '__main__':
    args = get_cmdline_args()
    if args.s:
        stream = open(args.s, b'r')
    else:
        stream = open('channels.yaml', b'r')

    data = load(stream, Loader=Loader)

    for bot in data['bots']:
        cmd = ["python"]
        cmd.append(os.path.join(os.path.dirname(__file__),"tgvkcontent.py"))
        cmd.append('-t')
        cmd.append(bot['token'])
        cmd.append('-c')
        cmd.extend([str(channel) for channel in bot['channels']])
        cmd.append('-g')
        cmd.extend([str(group) for group in bot['groups']])
        subprocess.Popen(" ".join(cmd), shell=True, cwd=os.path.dirname(__file__))

